var myApp = angular.module('myApp', ['ngRoute', 'ui.bootstrap']);


myApp.config(function ($routeProvider) {

    var checkRouting = function (loginService, $location) {

        if (!loginService.loginData.loggedIn) {
            $location.path("/login");
        } else {
            return true;
        }
    }

    $routeProvider.
    when('/home', {
        templateUrl: 'partials/home.html',
        /*resolve: {
            factory: checkRouting
        }*/
    }).
    when('/dashboard', {
        templateUrl: 'partials/dashboard.html',
        resolve: {
            factory: checkRouting
        }
    }).
    when('/cases', {
        templateUrl: 'partials/cases.html',
        resolve: {
            factory: checkRouting
        }
    }).
    when('/case/:id', {
        templateUrl: 'partials/case.html',
        controller: 'CaseController',
        resolve: {
            factory: checkRouting
        }
    }).
    when('/login', {
        templateUrl: 'partials/login.html',
        controller: 'LoginModalCtrl',
    }).
    otherwise({
        redirectTo: '/home'
    });



});

myApp.controller('ListController', ['$scope', 'db', 'loginService', '$location', function ($scope, db, loginService, $location) {

    $scope.cases = db.getCases();

    $scope.status = '';

    $scope.loginData = loginService.loginData;

    $scope.filterCases = function (action) {

        $scope.status = action;

    };

    $scope.loadCase = function (id) {
        $location.path('/case/' + id);
    };

}]);

myApp.controller('CarouselDemoCtrl', function ($scope) {

    $scope.slides = [{
        text: 'Virtual Doctor'
    }, {
        text: 'Virtual Doctor 2'
    }];

});

myApp.controller('CaseController', function (db, $scope, $routeParams, loginService) {

    $scope.slides = [];
    $scope.comments = [];

    $scope.newComment = {
        text: '',
        timeStamp: null,
        name: loginService.loginData.user,
        userId: loginService.loginData.userId
    };


    $scope.getCase = function () {

        $scope.newComment.text = '';
        $scope.newComment.timeStamp = null;

        db.getCase($routeParams.id).
        then(function (doc) {

            $scope.case = doc;

            $scope.comments = [];
            angular.forEach(doc.comments, function (comment) {

                //var text = decodeURI(comment.text);
                var text = comment.text;
                var avatar = null;

                if (comment.userId) {
                    avatar = comment.userId.substring(0, 1) + comment.userId.substring((comment.userId.length - 1)) + '.jpg';
                    avatar = avatar.toLowerCase();
                } else {
                    avatar = 'ff.jpg';
                }

                $scope.comments.push({
                    text: text,
                    avatar: avatar,
                    name: comment.name
                });

            });
            
            $scope.slides = [];
            angular.forEach(doc.imageUrls, function (imageUrl) {

                var url = 'http://ec2-46-137-64-81.eu-west-1.compute.amazonaws.com/vdoc_test/' + $routeParams.id + '/' + imageUrl.imageName;

                $scope.slides.push({
                    text: imageUrl.imageName,
                    src: url
                });
            });

        });
    };

    $scope.update = function () {

        var copy = angular.copy($scope.newComment)

        copy.timeStamp = new Date().getTime();
        //copy.text = escape($scope.newComment.text);
        if (!$scope.case.comments) {
            $scope.case.comments = [];
        }
        $scope.case.comments.push(copy);
        $scope.case.caseStatus = "Responded";

        db.updateCase($scope.case).success($scope.getCase);
    };

    // Toggle text areas
    $scope.responding = true;
    $scope.privateMode = false;

    $scope.enablePrivateMode = function () {

        $scope.responding = !$scope.responding;
        $scope.privateMode = !$scope.privateMode;
    };

    $scope.getCase();

});


myApp.factory('db', ['$http', function ($http) {

    var docs = [];
    var doc = null;

    return {

        getCases: function () {


            // Make http call to couch here and return some value
            $http.get('http://ec2-46-137-64-81.eu-west-1.compute.amazonaws.com/vdoc_test/_all_docs?include_docs=true').
            success(function (data, status, headers, config) {

                console.log(data);

                angular.forEach(data.rows, function (row) {

                    docs.push({
                        _id: row.doc._id,
                        patientId: row.doc.patientId,
                        status: row.doc.caseStatus,
                        description: row.doc.description,
                        createdDate: row.doc.createdDate
                    });
                });

            }).
            error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });

            return docs;

        },
        getCase: function (id) {

            return $http.get('http://ec2-46-137-64-81.eu-west-1.compute.amazonaws.com/vdoc_test/' + id).
            then(function (response) {
                return response.data;
            });
        },
        updateCase: function (doc) {

            return $http.put('http://ec2-46-137-64-81.eu-west-1.compute.amazonaws.com/vdoc_test/' + doc._id, doc);

            //            .
            //            then(function (response) {
            //
            //                if (response.status === 201) {
            //
            //                    alert('document updated');
            //                };
            //
            //            });

        }
    };
}]);