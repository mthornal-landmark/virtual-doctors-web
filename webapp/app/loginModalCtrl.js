myApp.controller('LoginModalCtrl', function ($scope, $modal, $log, loginService, $location) {
    
    $scope.loginData = loginService.loginData;

    $scope.open = function (size) {

        if ($scope.loginData.loggedIn) {

            loginService.loginData.text = "Login";
            loginService.loginData.user = "";
            loginService.loginData.loggedIn = false;
            localStorage.clear();
            $location.path("/home");

            
        } else {

            var modalInstance = $modal.open({
                templateUrl: 'partials/myModalContent.html',
                controller: 'ModalInstanceCtrl',
                size: size,
                resolve: {
                    credentials: function () {
                        return $scope.credentials;
                    }
                }
            });

            modalInstance.result.then(function (credentials) {

                $scope.loggedIn = loginService.login(credentials);

            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });

        }
    }

});

// Please note that $modalInstance represents a modal window (instance) dependency.
// It is not the same as the $modal service used above.

myApp.controller('ModalInstanceCtrl', function ($scope, $modalInstance, credentials) {

    $scope.credentials = {
        username: "",
        password: ""
    };

    $scope.ok = function () {
        $modalInstance.close($scope.credentials);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});
