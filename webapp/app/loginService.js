myApp.factory('loginService', ['$http', '$location', function ($http, $location) {

    var loginData = {
        user: "",
        userId: "",
        loggedIn: false,
        text: "Login"
    };

    var storedUser = localStorage.getItem('user');
    if (storedUser) {
        storedUser = JSON.parse(storedUser);
        loginData.user = storedUser.user;
        loginData.userId = storedUser.userId;
        loginData.loggedIn = true;
        loginData.text = "Logout";
    }

    var addUserToLocalStorage = function (user) {
        localStorage.setItem("user", JSON.stringify({
            user: user.name,
            userId: user.id
        }));
    }

    // Might use a resource here that returns a JSON array
    // Some fake testing data
    var clinicians = [{
            id: 'DaveC',
            name: 'Dave Clark',
            dummyText: 'Stuff and things',
            avatar: 'img/dc.jpg'
                            }, {
            id: 'PaulR',
            name: 'Paul Ross',
            dummyText: 'Love my cat!',
            avatar: ''
                            }, {
            id: 'JonathanB',
            name: 'Jonathan Brook',
            dummyText: "I'm cracking the whip",
            avatar: 'img/jb.jpg'
                            }, {
            id: 'JonB',
            name: 'Jonathan Brook',
            dummyText: "I'm cracking the whip",
            avatar: 'img/jb.jpg'
                            }, {
            id: 'MartinT',
            name: 'Martin Thornalley',
            dummyText: 'Dev ops wizard',
            avatar: 'img/mt.jpg'
                            }, {
            id: 'FranF',
            name: 'Fran Fieldhouse',
            dummyText: 'Clinical governance',
            avatar: 'img/ff.jpg'
                            }, {
            id: 'SamA',
            name: 'Sam Ashton',
            dummyText: 'Gimme the data!!',
            avatar: 'img/sa.jpg'
                            }, {
            id: 'ChristopherK',
            name: 'Christopher Keeley',
            dummyText: 'Receding ... moi?',
            avatar: 'img/ck.jpg'
                            }, {
            id: 'ChrisK',
            name: 'Christopher Keeley',
            dummyText: 'Receding ... moi?',
            avatar: 'img/ck.jpg'
                            }, {
            id: 'IanC',
            name: 'Ian Clarke',
            dummyText: 'The man with the plan',
            avatar: 'img/ic.png'
                            }, {
            id: 'HuwJ',
            name: 'Huw Jones',
            dummyText: 'Executive Director',
            avatar: ''
                            }, {
            id: 'OliviaB',
            name: 'Olivia Bell',
            dummyText: 'The real Clinician!',
            avatar: ''
                            }, {
            id: 'NickW',
            name: 'Nick Ward',
            dummyText: 'Code jockey .....',
            avatar: ''
                            }, {
            id: 'TinaC',
            name: 'Tina Coomber',
            dummyText: 'Test test and test again!',
            avatar: 'img/tc.jpg'
                            }, {
            id: 'KarlB',
            name: 'Karl Bailey',
            dummyText: 'Azure? ... pah!',
            avatar: 'img/kb.jpg'
                            }, {
            id: 'MichelleB',
            name: 'Michelle Baker',
            dummyText: "Feed em pizza, that'll work",
            avatar: 'img/mb.jpg'
                            }
                        ];

    return {

        loginData: loginData,
        login: function (credentials) {

            angular.forEach(clinicians, function (user) {

                if (user.id.toLowerCase() === credentials.username.toLowerCase()) {
                    loginData.user = user.name;
                    loginData.userId = user.id;
                    loginData.loggedIn = true;
                    loginData.text = "Logout";

                    addUserToLocalStorage(user);

                    $location.path("/cases");

                    return true;
                }
            });
        },


    };

            }]);
